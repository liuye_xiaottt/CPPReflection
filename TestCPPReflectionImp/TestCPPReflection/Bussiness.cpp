#include "Bussiness.h"
#include "common.h"
#include <iostream>
#include "WindowsFactory.h"
#include "MacFactory.h"


namespace DependencyLocate
{
	class FactoryContainer
	{
	public:
		static IFactory* factory;

		static void InitInstace()
		{
			string value = "Windows";
			if ("Windows" == value)
			{
				factory = new WindowsFactory();
			}
			else if ("Mac" == value)
			{
				factory = new MacFactory();
			}
			else
			{
				factory = NULL;
			}
		}
	};
	IFactory* FactoryContainer::factory = NULL;
}

#include "CreateInstance.h"
namespace DependencyLocate
{
	void Program::testMain()
	{
		FactoryContainer::InitInstace();
		IFactory* factory = FactoryContainer::factory;
		IWindow* window1 = factory->MakeWindow();
		cout << "----------------------"<< endl;
		IWindow* window = CreateInstance::CreateWindow("MacWindow");
		cout<<"创建 " + window->ShowInfo()<<endl;
		IWindow* windowOther = CreateInstance::CreateWindow("WindowsWindow");
		cout << "创建 " + windowOther->ShowInfo() << endl;
		windowOther = CreateInstance::CreateWindow("WindowsWindow");
		cout << "创建0000 " + windowOther->ShowInfo() << endl;

		IButton* button = factory->MakeButton();
		cout << ("创建 " + button->ShowInfo())<<endl;

		IButton* buttonC = CreateInstance::CreateButton("MacButton");
		cout << ("创建 " + buttonC->ShowInfo()) << endl;

		
		}
};
