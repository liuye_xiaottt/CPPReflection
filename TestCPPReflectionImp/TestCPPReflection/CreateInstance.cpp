#include "CreateInstance.h"
#include "IWindow.h"
#include "Reflactor.h"
using namespace::DependencyLocate;

CreateInstance::CreateInstance()
{
}

CreateInstance::~CreateInstance()
{
	ClassMapT<IWindow>::Release();
}

template<typename T>
map<string, pf> ClassMapT<T>::m_ClassMap;

template<typename T>
std::shared_ptr<T> ClassMapT<T>::m_sharedPtr;

IWindow* CreateInstance::CreateWindow(string className)
{
	IWindow * pObj = ClassMapT<IWindow>::forName(className);
	return pObj;
}

IButton* CreateInstance::CreateButton(string className)
{
	IButton * pObj = ClassMapT<IButton>::forName(className);
	return pObj;
}
