#pragma once
#include "common.h"
#include "IButton.h"

namespace DependencyLocate
{
	class WindowsButton : public IButton
	{
	public:

		WindowsButton();
		~WindowsButton(){}
		string ShowInfo();
	private:
		string Description;
	};
}