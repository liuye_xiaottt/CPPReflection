#pragma once
#include "common.h"

namespace DependencyLocate
{
	class IWindow;
	class IButton;
	class CreateInstance
	{
	public:
		CreateInstance();
		static IWindow* CreateWindow(string className);
		static IButton* CreateButton(string className);
		~CreateInstance();
	};

};
