#pragma once
#include "common.h"
#include "IWindow.h"
#include "IButton.h"

namespace DependencyLocate
{
	interface IFactory
	{
	public:
		virtual IWindow* MakeWindow() = 0;

		virtual IButton* MakeButton() = 0;
	};
}