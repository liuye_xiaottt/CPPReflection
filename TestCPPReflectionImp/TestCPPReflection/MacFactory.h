#pragma once
#include "IFactory.h"


namespace DependencyLocate
{
	class MacFactory : public IFactory
	{
	public:
		IWindow* MakeWindow();

		IButton* MakeButton();
	};
}
