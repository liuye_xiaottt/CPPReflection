#pragma once
#include "common.h"
#include <vector>
#include <map>
#include <iostream>

namespace DependencyLocate
{
	using namespace::std;
	interface IWindow
	{
	public:
		virtual string ShowInfo() = 0;
		virtual ~IWindow(){ cout << "IWindows destructor" << endl; }
	};
}