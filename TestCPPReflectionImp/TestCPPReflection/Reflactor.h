#pragma once
#include "IWindow.h"
#include <map>
#include <memory>
using namespace::std;
namespace DependencyLocate
{
	typedef void* (*pf)();

	template<class T>
	class ClassMapT
	{
		typedef T* (*pRealType)();
	private:
		/**
		* @brief 私有构造函数
		* 为了让这个类不产生实例对象
		*/

		/**
		* @brief 以类名为键的一个存放着构建方法的Hash表
		*/
		static map<string, pf> m_ClassMap;
		static std::shared_ptr<T> m_sharedPtr;
	public:
		/**
		* @brief 定义一个类必须注册一下
		* @param _className 类名
		* @param _createFun 注册函数指针
		*/
		static void RegistClass(string _className, pf _createFun)
		{
			if (m_ClassMap.find(_className) != m_ClassMap.end())
				return; //已经注册过，直接返回

			m_ClassMap[_className] = _createFun;
		}
		static T* forName(string _className)
		{
			if (m_ClassMap.find(_className) == m_ClassMap.end())
				return NULL;    //没有找到类对象，返回NULL
			else
			{
				T* pT = ((pRealType)m_ClassMap[_className])();
				m_sharedPtr.reset(pT);
				return m_sharedPtr.get();
				return ((pRealType)m_ClassMap[_className])();
				//return (m_ClassMap[_className])();
			}
		}
		static void Release()
		{
			m_sharedPtr.reset();
		}
	};
	
	template<class realT, typename T>
	class DelegatObject
	{
	public:
		/**
		* @brief 构造函数
		* @param _className 类名
		*/
		DelegatObject(string _className)
		{
			ClassMapT<realT>::RegistClass(_className, (pf)&(DelegatObject::Create));
		}

		/**
		* @brief 创建实例函数
		*/
		static realT* Create()
		{
			return static_cast<realT*>(new T);
		}
	};
}
#ifndef REGIST_CLASS
#define REGIST_CLASS(T, X) DelegatObject<T, X> __class_##X( #X )
#endif