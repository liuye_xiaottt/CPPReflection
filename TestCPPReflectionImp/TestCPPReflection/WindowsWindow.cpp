#include "Reflactor.h"
#include "WindowsWindow.h"
#include <iostream>
using namespace::DependencyLocate;

REGIST_CLASS(IWindow, WindowsWindow);
string WindowsWindow::ShowInfo()
{
	return "WindowsWindow";
}

WindowsWindow::WindowsWindow()
{
	cout << "WindowsWindow constructor" << endl;
}

WindowsWindow::~WindowsWindow()
{
	cout << "WindowsWindow destructor" << endl;
}