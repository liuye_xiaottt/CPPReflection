#pragma once
#include "common.h"
#include "IWindow.h"
namespace DependencyLocate
{
	class WindowsWindow : public IWindow
	{
	public:
		WindowsWindow();
		~WindowsWindow();
		string ShowInfo();
	};
}
