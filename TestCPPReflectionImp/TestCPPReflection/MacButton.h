#pragma once
#include "common.h"
#include "IButton.h"

namespace DependencyLocate
{

	class MacButton : public IButton
	{
	public:

		MacButton();
		~MacButton(){}
		string ShowInfo();
	private:
		string Description;
	};
}